.PHONY: build

build:
	docker build --no-cache -t pxlfsn/php7-nginx .

run:
	docker run --rm --name test -p 8080:80 -t pxlfsn/php7-nginx

exec:
	docker exec -it test /bin/sh
