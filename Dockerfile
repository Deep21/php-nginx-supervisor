FROM php:7.1-fpm-alpine3.9

ENV PACKAGES="nginx py-pip vim git"

RUN apk update \
    && apk --no-cache --progress add $PACKAGES \
    && mkdir -p /run/nginx/ \
    && chmod ugo+w /run/nginx/ \
    && rm -f /etc/nginx/nginx.conf \
    && mkdir -p /etc/nginx/conf.d \
    && chmod -R 775 /var/www/ \
    && chown -R nginx:nginx /var/www/ \
    && pip install --upgrade pip \
    && pip install supervisor\
    && rm /usr/local/etc/php-fpm.d/zz-docker.conf \
    && docker-php-ext-install pdo_mysql

RUN apk add --no-cache icu-dev
RUN docker-php-ext-install intl
RUN docker-php-ext-install bcmath mysqli pdo_mysql 
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN mkdir /app
WORKDIR /app
RUN git clone https://github.com/dddinphp/blog-cqrs.git .
RUN composer install --no-dev 
RUN chmod -R 777 /app && chown -R nginx:nginx /app

COPY ./manifest/ /

EXPOSE 80

CMD ["/bin/sh", "/entrypoint.sh"]
